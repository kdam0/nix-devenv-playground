Install `nix`:
```
# linux
sh <(curl -L https://nixos.org/nix/install) --daemon
# wsl2
sh <(curl -L https://nixos.org/nix/install) --no-daemon
# macos
sh <(curl -L https://nixos.org/nix/install)
```

Enable `flakes` support by adding to `$HOME/.config/nix/nix.conf`:
```
experimental-features = nix-command flakes
```

Then, install `direnv`:
```
# debian
sudo apt install direnv
# macos
brew install direnv
```

Hook `direnv` to your end of your shell rc file:
```
# for bash
eval "$(direnv hook bash)"
# for zsh
eval "$(direnv hook zsh)"
```

Enable automatic nix shell activation for this dir:
```
direnv allow
```
