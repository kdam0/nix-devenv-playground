{
  description = "Description for the project";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    devenv.url = "github:cachix/devenv";
  };

  outputs = inputs@{ flake-parts, nixpkgs, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.devenv.flakeModule
      ];
      systems = nixpkgs.lib.systems.flakeExposed;
      perSystem = { config, self', inputs', pkgs, system, ... }: {

        packages.default = pkgs.hello;

        devenv.shells.projectA = {
          packages = [ config.packages.default  pkgs.jq ];
          languages.python.enable = true;
          languages.python.poetry.enable = true;
          languages.python.poetry.activate.enable = true;
          enterShell = ''
            echo this is a Django project
            pip list
          '';
        };

        devenv.shells.projectB = {
          packages = [ config.packages.default ];
          languages.python.enable = true;
          languages.python.poetry.enable = true;
          languages.python.poetry.activate.enable = true;
          enterShell = ''
            echo this is an ansible project
          '';
        };

        devenv.shells.projectC = {
          packages = [ config.packages.default ];
          languages.opentofu.enable = true;
          enterShell = ''
            echo this is a terraform project
          '';
        };
      };
    };
}
